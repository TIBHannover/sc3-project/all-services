#!/bin/bash

git clone https://gitlab.com/TIBHannover/sc3-project/backend.git
git clone https://gitlab.com/TIBHannover/sc3-project/frontend.git
git clone https://gitlab.com/TIBHannover/sc3-project/dr-owl-api.git
git clone https://gitlab.com/TIBHannover/sc3-project/ontologyprocessingservice.git

BUILD_TYPE='dev'

sh ./buildCLI.sh && 
sh ./buildBackend.sh && 
sh ./buildFrontend.sh && 
sh ./buildProcessingService.sh

echo "*****************"
echo "*****************"
echo "ADJUST THE .env FILES in "
echo "backend/" 
echo "frontend/server/"
echo ">>>> GITHUB_CLIENT_ID = <<<"
echo ">>>> GITHUB_CLIENT_SECRET <<<"
echo "**************************************"

echo "You need to manually start each service:" 
echo "starting the backend fist" 
echo "      cd backend " 
echo "      docker-compose up -d postgres_sc3_backend " 
echo "      flask db upgrade && python3 app.py " 
echo "starting the ontology processing service" 
echo "      cd ontologyprocessingservice " 
echo "      python3 flask-proxy.py " 
echo "starting the frontend" 
echo "      cd frontend " 
echo "      npm start " 

echo "*****************"
echo "*****************"