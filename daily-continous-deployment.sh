#!/bin/bash

cd backend
git pull
cd ..

cd frontend
git pull
cd ..

cd ontologyprocessingservice
git pull
cd ..

docker-compose build data_infrastruct
docker-compose build processing
docker-compose build frontend

docker-compose up -d data_infrastruct
docker-compose up -d processing
docker-compose up -d frontend