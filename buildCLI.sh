#!/bin/bash

echo "building CLI for ontology processing"
cd dr-owl-api 
sh dockerizedBuilder.sh
cp ./owlapi-cli.jar ../ontologyprocessingservice 
cp ./owl2vowl.jar ../ontologyprocessingservice 
cd ..
cd ontologyprocessingservice
sh ./testing.sh
echo ""
echo ""
echo "^^^^^^^^ SOME results to see if CLI jar works"
cd ..


