@echo off

set BUILD_TYPE="dev"

cd windows-script
call cloneRepos.bat
cd windows-script
call buildCLI.bat
cd windows-script
call buildBackend.bat
cd windows-script
call buildFrontend.bat
cd windows-script
call buildProcessingService.bat

echo "*****************"
echo "*****************"
echo "ADJUST THE .env FILES in "
echo "backend/" 
echo "frontend/server/"
echo ">>>> GITHUB_CLIENT_ID = <<<"
echo ">>>> GITHUB_CLIENT_SECRET <<<"
echo "**************************************"

echo "You need to manually start each service:" 
echo "starting the backend fist" 
echo "      cd backend " 
echo "      docker-compose up -d postgres_sc3_backend " 
echo "      flask db upgrade && python3 app.py " 
echo "starting the ontology processing service" 
echo "      cd ontologyprocessingservice " 
echo "      python3 flask-proxy.py " 
echo "starting the frontend" 
echo "      cd frontend " 
echo "      npm start " 

echo "*****************"
echo "*****************"
