#!/bin/bash

git clone https://gitlab.com/TIBHannover/sc3-project/backend.git
git clone https://gitlab.com/TIBHannover/sc3-project/frontend.git
git clone https://gitlab.com/TIBHannover/sc3-project/dr-owl-api.git
git clone https://gitlab.com/TIBHannover/sc3-project/ontologyprocessingservice.git
