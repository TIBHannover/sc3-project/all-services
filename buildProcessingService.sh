#!/bin/bash
echo "INITIALIZING ONTOLOGY PROCESSING SERVICE" 


#this is only for my machine which also has version 2 installed 
cd ontologyprocessingservice
PYTHON_VERSION=`python -c 'import sys; version=sys.version_info[:1]; print("{0}".format(*version))'`
echo "PYTHON VERSION"
echo $PYTHON_VERSION

if [ $PYTHON_VERSION != 3 ]; then
	echo "WE NEED PYTHON 3"
	python3 -m venv p3env
	. ./p3env/bin/activate
	python --version

fi
python -m pip install -U pip
pip install -r requirements.txt
sh initializeEnv.sh
cd ..

