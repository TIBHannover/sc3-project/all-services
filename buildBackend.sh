#!/bin/bash
echo "INITIALIZING BACKEND" 


#this is only for my machine which also has version 2 installed 
cd backend
PYTHON_VERSION=`python -c 'import sys; version=sys.version_info[:1]; print("{0}".format(*version))'`
echo "PYTHON VERSION"
echo $PYTHON_VERSION

if [ $PYTHON_VERSION != 3 ]; then
	echo "WE NEED PYTHON 3"
	python3 -m venv p3env
	. ./p3env/bin/activate
	python --version

fi
python -m pip install -U pip
pip install -r requirements.txt
sh initializeEnv.sh 
docker network create network_sc3_backend

# we dont need this because we have the mogration folder already
#docker-compose up -d postgres_sc3_backend 
#flask db init 
#flask db migrate 
#flask db upgrade 
#docker-compose down 

cd ..

