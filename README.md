# All-Services

This is repo provides a script that will download all services and initialize them depending on some variables

<b>Note:</b> This is tested only on Linux 

On Linux run `bash downloadAndInitialize.sh` 

This will download all git repos and initialize them.


We are currently in the development stage, so we are using the build_type=development.

This means we will have to start the services manually<br/>



You need to manually start each service:<br/>
starting the backend fist<br/>
  cd backend <br/>
  docker-compose up -d postgres_sc3_backend <br/>
  python3 app.py <br/>
starting the ontology processing service <br/>
  cd ontologyprocessingservice <br/>
  python3 flask-proxy.py <br/>
starting the frontend <br/>
  cd frontend <br/>
  npm start <br/>


<b>For more details, please refere to the individualrepos.</b> 

## Automatic Installation / Update

### Run it locally with VirtualBox

Scenario: To test the application a little bit with minimal effort, you can use a local installation in the local VirtualBox VM via vargant. 

Prerequisites
* [Git](https://git-scm.com/downloads)
* [Vagrant](https://www.vagrantup.com/downloads.html)
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

Perform the following steps in the terminal (Linux / macOS) or in the GitBash (Windows).
```
git clone https://gitlab.com/TIBHannover/sc3-project/all-services.git
cd all-services
vagrant up
```

You can stop the virtual machine with
```
vagrant halt
```
... and start again with
```
vagrant up
```
If you want to reload the configuration, perform
```
vagrant reload --provision
```

The VM can then simply be thrown away at any time with `vagrant destroy`.

### Direct installation

Scenario: Install the application on an existing system directly via the ansible-playbook. Note for developers: this method is also suitable for installations on the VirtualBox VM. Since you can work with your own inventory file, you can test the complete custumizations of your production environment on the VM without having to make changes in the _group_vars_ of the Vagrant Box.

* requirements:
     * [ansible](https://docs.ansible.com/) installed on the local computer
* clone project
* install ansible galaxy roles:
     * ```ansible-galaxy install -r requirements.yml```
* create ansible inventory inventory.yml (see [inventory-local.yml](inventory-local.yml)) and adjust all variables to your installation (see variables in _ansible/group_vars_)
* run ```ansible-playbook -v -i inventory.yml ansible/sc3.yml```
