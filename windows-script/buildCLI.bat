@echo off
echo "building CLI for ontology processing" 
cd ..
cd dr-owl-api
call dockerizedBuilder.bat
copy owlapi-cli.jar ..\ontologyprocessingservice\
copy owl2vowl.jar ..\ontologyprocessingservice\
cd ..
cd ontologyprocessingservice
call testing.bat
echo ""
echo ""
echo "^^^^^^^^ SOME results to see if CLI jar works"
cd ..