@echo off
echo "INITIALIZING BACKEND" 
cd ..
cd backend

python -m pip install -U pip
pip install -r requirements.txt
call initializeEnv.bat
docker network create network_sc3_backend
docker-compose up -d postgres_sc3_backend 
flask db init 
flask db migrate 
flask db upgrade 
docker-compose down 
cd ..

