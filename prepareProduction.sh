#!/bin/bash

sh cloneRepos.sh

#this is currently testing
cd backend 
#git checkout 6-add-_migrations-to-git-repo
sh initializeEnv.sh
cd ..


#prepare fronend
cd frontend
sh initializeEnv.sh
cd server
sh initializeEnv.sh
cd ..
cd ..

#prepare CLI and copy to processing
cd dr-owl-api
sh dockerizedBuilder.sh
mv owlapi-cli.jar ../ontologyprocessingservice/owlapi-cli.jar
cd ..
cd ontologyprocessingservice
sh initializeEnv.sh